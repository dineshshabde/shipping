<?php

class Database
{
    private $conn = null;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        // Read from properties file
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "shipping";
        try {
            $this->conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Error : couldn't establish database connection " . $e->getMessage();
        }
    }

    public function select_data($customer_id)
    {
        if ($customer_id != null) {

        } else {
            try {
                $query = 'SELECT * FROM customer_address';
                $PDOStatement = $this->conn->prepare($query);
                if ($PDOStatement->execute()) {

                    return $PDOStatement->fetch(\PDO::FETCH_ASSOC);
                } else {
                    return false;
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }

        }
        return false;
    }
}

$database = new Database();