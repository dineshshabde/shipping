$(document).ready(function () {
    $('#shipping_form').validate({
        rules: {
            origin_zip_code: {
                minlength: 8,
                required: true
            },
            destination_zip_code: {
                minlength: 8,
                required: true
            },
            weight: {
                required: true,
                number: true
            },
            volume_type: {
                minlength: 2,
                required: true
            },
            cost_of_goods: {
                required: true,
                number: true
            },
            width: {
                required: true,
                number: true
            },
            height: {
                required: true,
                number: true
            },
            length: {
                required: true,
                number: true
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $("#calculate_shipping").attr("disabled", true);
            $.ajax({
                url: 'calc.php',
                data: {
                    "origin_zip_code": $('#origin_zip_code').val(),
                    "destination_zip_code": $('#destination_zip_code').val(),
                    "weight": $('#weight').val(),
                    "volume_type": $('#volume_type').val(),
                    "cost_of_goods": $('#cost_of_goods').val(),
                    "width": $('#width').val(),
                    "height": $('#height').val(),
                    "length": $('#length').val()
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (response) {
                    $("#calculate_shipping").attr("disabled", false);
                    if (response.content != undefined) {
                        console.log("response.content.delivery_options", response.content.delivery_options);
                        $.each(response.content.delivery_options, function (i, item) {
                            var $tr = $('<tr>').append(
                                $('<td><input type="checkbox" name="shipping_option[]" id="' + item.delivery_method_id + '">'),
                                $('<td>').text(item.description + ', ' + item.final_shipping_cost + '$,' + item.delivery_estimate_business_days + ' Day(s)')
                            );
                            $tr.appendTo('#shipping-table-data');
                        });
                    } else {
                        console.error("Check value");
                    }

                }
            });
        }
    });

});
