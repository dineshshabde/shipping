## How to use
1. Use the DDL under schema directory.
2. Change username and password in config.php

## Implementation

 - [x] Create UI
 - [x] Add form and validation
 - [x] Implement ajax call upon submit with validation
 - [x] Implement PHP curl to get response from API
 - [x] Upon successful response, populate it in a table and let the user choose from given option.
 - [ ] Get one option from user about the proposed cost  
 - [ ] Implement last used date logic
 - [ ] Insert into database


1. API key also has been committed, since it will be disable after code verification as per email content 
2. bower_components folder should never be tracked by version control. The contents should be loaded by the cli in respective machine