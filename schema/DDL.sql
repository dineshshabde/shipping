CREATE SCHEMA `shipping`
  DEFAULT CHARACTER SET utf8;

CREATE TABLE `shipping`.`customer_address` (
  `id`           INT         NOT NULL AUTO_INCREMENT,
  `customer_id`  INT         NOT NULL,
  `address_name` VARCHAR(45) NOT NULL,
  `type`         VARCHAR(8)  NOT NULL,
  `main`         INT(0)      NOT NULL,
  PRIMARY KEY (`id`)
);
/*
Create table to store both billing & shipping address
Select value based on last insert date / last updated date
*/