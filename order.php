<?php

/**
 * Created by PhpStorm.
 * User: dinesh
 * Date: 18/04/17
 * Time: 7:20 AM
 */
class order
{
    var $origin_zip_code;
    var $destination_zip_code;
    var $weight;
    var $cost_of_goods;
    var $width;
    var $height;
    var $length;

    /**
     * order constructor.
     * @param $origin_zip_code
     * @param $destination_zip_code
     * @param $weight
     * @param $cost_of_goods
     * @param $width
     * @param $height
     * @param $length
     */
    public function __construct($origin_zip_code, $destination_zip_code, $weight, $cost_of_goods, $width, $height, $length)
    {
        $this->origin_zip_code = $origin_zip_code;
        $this->destination_zip_code = $destination_zip_code;
        $this->weight = $weight;
        $this->cost_of_goods = $cost_of_goods;
        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
    }

    /**
     * @return mixed
     */
    public function getOriginZipCode()
    {
        return $this->origin_zip_code;
    }

    /**
     * @param mixed $origin_zip_code
     */
    public function setOriginZipCode($origin_zip_code)
    {
        $this->origin_zip_code = $origin_zip_code;
    }

    /**
     * @return mixed
     */
    public function getDestinationZipCode()
    {
        return $this->destination_zip_code;
    }

    /**
     * @param mixed $destination_zip_code
     */
    public function setDestinationZipCode($destination_zip_code)
    {
        $this->destination_zip_code = $destination_zip_code;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getCostOfGoods()
    {
        return $this->cost_of_goods;
    }

    /**
     * @param mixed $cost_of_goods
     */
    public function setCostOfGoods($cost_of_goods)
    {
        $this->cost_of_goods = $cost_of_goods;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

}

$order = new order();