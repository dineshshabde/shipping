<?php
include_once 'config.php';
$database->select_data(null);
?>
<html>
<head>
    <title>Homepage</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- bootstrap and other scripts -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://use.fontawesome.com/2f9d1e37c0.js"></script>
    <script src="script/app.js"></script>
    <script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3>
                <legend>Print Shipping</legend>
            </h3>
        </div>
        <div class="col-sm-5">
            <h4>Shipping details:</h4>
            <div class="panel panel-default">
                <div class="panel-body form-horizontal payment-form">
                    <form id="shipping_form">
                        <div class="form-group">
                            <label for="origin_zip_code" class="col-sm-3 control-label">Origin zip code​</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="origin_zip_code" name="origin_zip_code"
                                       value="04012-090">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="destination_zip_code" class="col-sm-3 control-label">Destination zip
                                code​</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="destination_zip_code"
                                       name="destination_zip_code" value="111111111">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="weight" class="col-sm-3 control-label">Weight​</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="weight" name="weight" value="0.5">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="volume_type" class="col-sm-3 control-label">Volume Type</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="volume_type" name="volume_type" value="BOX">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cost_of_goods" class="col-sm-3 control-label">Cost of goods​</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="cost_of_goods" name="cost_of_goods"
                                       value="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="width" class="col-sm-3 control-label">Width​</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="width" name="width" value="10">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="height" class="col-sm-3 control-label">Height​</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="height" name="height" value="10">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="length" class="col-sm-3 control-label">Length​</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="length" name="length" value="25">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <button type="submit" class="btn btn-default btn-success preview-add-button"
                                        id="calculate_shipping">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Calculate
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-7">
            <h4>Select Shipping:</h4>
            <form name="place_order" id="place_order" action="">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table" id="shipping-table-data">
                                <thead>
                                <tr>
                                    <th>Option</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <hr style="border:1px dashed #dddddd;">
                        <button type="submit" class="btn btn-primary btn-block">Select and proceed</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
