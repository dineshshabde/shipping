<?php
$curl_post_data = array(
    "origin_zip_code"=>$_POST['origin_zip_code'],
    "destination_zip_code"=>$_POST['destination_zip_code'],
    "volumes"   => array([
        "weight"=>trim($_POST['weight']),
        "volume_type"=>trim($_POST['volume_type']),
        "cost_of_goods"=>trim($_POST['cost_of_goods']),
        "width"=>trim($_POST['width']),
        "height"=>trim($_POST['height']),
        "length"=>trim($_POST['length']),
    ]),
);
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.intelipost.com.br/api/v1/quote",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => json_encode($curl_post_data),
    CURLOPT_HTTPHEADER => array(
        "api_key: 9009f95101bf48b01a50928a2a71ed1ae9083fc1d3c08439b0613dfc38e656c5",
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "platform: intelipost-docs",
    ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}
?>